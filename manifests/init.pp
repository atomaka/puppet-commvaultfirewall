#
class commvaultfirewall($backnet = false) {
  if($backnet == false) {
    fail('You must enter your the appropriate backnet VLAN number')
  }

  # public server
  firewall { '900 accept commvault':
    proto  => 'tcp',
    dport  => ['8400-8407','8600-8619'],
    source => '35.8.113.12',
    action => 'accept',
  }

  # backnet servers
  firewall { '901 accept commvault':
    proto  => 'tcp',
    dport  => ['8400-8407','8600-8619'],
    source => "172.16.${backnet}.1",
    action => 'accept',
  }
  firewall { '902 accept commvault':
    proto  => 'tcp',
    dport  => ['8400-8407','8600-8619'],
    source => "172.16.${backnet}.2",
    action => 'accept',
  }
  firewall { '903 accept commvault':
    proto  => 'tcp',
    dport  => ['8400-8407','8600-8619'],
    source => "172.16.${backnet}.11",
    action => 'accept',
  }
  firewall { '904 accept commvault':
    proto  => 'tcp',
    dport  => ['8400-8407','8600-8619'],
    source => "172.16.${backnet}.12",
    action => 'accept',
  }
}
